# wp-assignment
WP - Batch API Consumer Assignment

A Webservice (REST or other) which accepts a CSV file and fills out these missing fields - if possible - using *WhitePages Pro API*, and make them available for download after the job finished.