angular.module('app', ['angularFileUpload'])
.controller('MainController', function ($scope, FileUploader) {
	
	$scope.success = function (item, response, status, headers) {
		console.log(item, response, status, headers);
		item.link = headers.location;
	};

	var uploader = $scope.uploader = new FileUploader({
        url: '/upload',
        onSuccessItem: $scope.success
    });
});