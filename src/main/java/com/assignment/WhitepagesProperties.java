package com.assignment;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configurable RESTful service endpoint properties
 * populated from application.yml
 * 
 * @author richteri
 *
 */
@ConfigurationProperties(prefix = "whitepages", ignoreUnknownFields = false)
public class WhitepagesProperties {
	private final Api api = new Api();
	
	public static class Api {

		private SearchBy searchBy = new SearchBy();
		private String key;
		private String baseUrl;
		private String version;

		public static class SearchBy {
			private String phone;
			private String location;
			private String person;

			public String getPhone() {
				return phone;
			}

			public void setPhone(String phone) {
				this.phone = phone;
			}

			public String getLocation() {
				return location;
			}

			public void setLocation(String location) {
				this.location = location;
			}

			public String getPerson() {
				return person;
			}

			public void setPerson(String person) {
				this.person = person;
			}

		}

		public SearchBy getSearchBy() {
			return searchBy;
		}

		public void setSearchBy(SearchBy searchBy) {
			this.searchBy = searchBy;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getBaseUrl() {
			return baseUrl;
		}

		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}
	}

	public Api getApi() {
		return api;
	}

}
