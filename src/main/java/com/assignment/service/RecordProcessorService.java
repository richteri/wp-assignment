package com.assignment.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Record;
import com.assignment.query.GenericQuery;
import com.assignment.query.QueryBuilder;
import com.assignment.util.RecordProcessor;

/**
 * Service that provides access to the WhitePages Pro API
 * by asynchronous calls
 * @author richteri
 *
 */
@Service
public class RecordProcessorService {
	
    private final Logger log = LoggerFactory.getLogger(RecordProcessorService.class);
    
	RestTemplate restTemplate = new RestTemplate();	
	
	@Autowired
	private WhitepagesProperties properties;
	
	/**
	 * Processes single record asynchronously
	 * @param record the record to process
	 * @param processor the record processor
	 * @return async promise
	 * @throws InterruptedException
	 */
	@Async
    public Future<?> processRecord(Record record, RecordProcessor processor) throws InterruptedException {
		
		List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
		
		if (log.isDebugEnabled()) {
		    log.debug("Built queries {} for record {}", 
		            queries.stream()
    		            .map(q -> q.buildUri().toString())
    		            .collect(Collectors.joining(", ")),
		            Arrays.toString(record.getRecord()));
		}
		
		try {			
			for (GenericQuery<?> query : queries) {
			
			    if (log.isDebugEnabled()) {
			        log.debug("Executing query {}", query.buildUri());
			    }
				
			    query.execute(restTemplate);
				if (record.getMissingFields().isEmpty()) {
				    
	                if (log.isDebugEnabled()) {
	                    log.debug("Populated all fields for record {}", Arrays.toString(record.getRecord()));
	                }
					
	                break;
				}
			}
			processor.writeRecord(record);

			if (log.isDebugEnabled()) {
                log.debug("Record persisted {}", Arrays.toString(record.getRecord()));
            }
			
		} catch (Exception e) {
			// log error
			// if something failed, write out the original record
			processor.writeRecord(record);
			
            log.warn("Record persisted with error {}, {}", e.getMessage(), Arrays.toString(record.getRecord()));
		}
		
        if (log.isDebugEnabled()) {
            log.debug("Record processing finished {}", Arrays.toString(record.getRecord()));
        }

        return new AsyncResult<>(null);
    }
}
