package com.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableConfigurationProperties({ WhitepagesProperties.class })
@EnableAsync
public class AssignmentApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AssignmentApplication.class, args);
	}
}
