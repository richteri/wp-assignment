package com.assignment.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.assignment.util.AddressParser;

/**
 * CSV record with mapping information and helper functions
 * @author richteri
 *
 */
public class Record {
	private final Map<Column, Integer> mapping;
	private final String record[];

	public Record(Map<Column, Integer> mapping, String[] record) {
		super();
		this.mapping = mapping;
		this.record = record;
	}

	/**
	 * Gets the underlying string array
	 * @return record
	 */
	public String[] getRecord() {
		return record;
	}

	/**
	 * Checks whether a specific property is set
	 * @param column the property selector
	 * @return true if a property has value
	 */
	public boolean has(Column column) {
		return mapping.containsKey(column) && !record[mapping.get(column)].trim().isEmpty();
	}
	
	/**
	 * Gets a specific property value
	 * @param column the property selector
	 * @return value of the property
	 */
	public String get(Column column) {
		return has(column) ? record[mapping.get(column)].trim() : "";
	}
	
	/**
	 * Sets the value of a specific column. Does not write over existing values.
	 * @param column the property selector
	 * @param value the value to set
	 */
	public void set(Column column, String value) {
		if (mapping.containsKey(column) && record[mapping.get(column)].trim().isEmpty()) {
			record[mapping.get(column)] = value;
		}
	}
	
	/**
	 * Helper for reading the city property
	 * @return city
	 */
	public String getCity() {
		if (has(Column.CITY)) {
			return get(Column.CITY);
		}
		
		return "";
	}
	
	/**
	 * Helper for reading the state property.
	 * If no state property is present, tries to parse the address property.
	 * @return state
	 */
	public String getState() {
		if (has(Column.STATE)) {
			return get(Column.STATE);
		}
		
		if (has(Column.ADDRESS)) {
			return AddressParser.extractState(get(Column.ADDRESS));
		}
		
		return "";
	}
	
	/**
	 * Helper for reading the address property
	 * @return address
	 */
	public String getAddress() {
		if (has(Column.ADDRESS)) {
			return AddressParser.extractAddress(get(Column.ADDRESS));
		}
		
		return "";
	}

	/**
	 * Helper for reading the zip property.
	 * If no zip property is present, tries to parse the address property.
	 * @return zip
	 */
	public String getZip() {
		if (has(Column.ZIP)) {
			return get(Column.ZIP);
		}
		
		if (has(Column.ADDRESS)) {
			return AddressParser.extractZip(get(Column.ADDRESS));
		}
		
		return "";
	}
	
	/**
	 * Convenience setter for the address property
	 * @param city
	 * @param state
	 * @param zip
	 */
	public void setAddress(String city, String state, String zip) {
		set(Column.CITY, city);
		set(Column.STATE, state);
		set(Column.ZIP, zip);
		set(Column.ADDRESS, String.format("%s, %s %s", city, state, zip));
	}
	
	/**
	 * Lists the missing properties
	 * @return list
	 */
	public List<Column> getMissingFields() {
		List<Column> fields = new ArrayList<>();
		for (Column column : mapping.keySet()) {
			if (!has(column)) {
				fields.add(column);
			}
		}
		return fields;
	}
	
	/**
	 * Lists the properties with values
	 * @return list
	 */
	public List<Column> getNonEmptyFields() {
		List<Column> fields = new ArrayList<>();
		for (Column column : mapping.keySet()) {
			if (has(column)) {
				fields.add(column);
			}
		}
		return fields;
	}
}
