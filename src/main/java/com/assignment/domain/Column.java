package com.assignment.domain;

/**
 * Enumeration of processed column names
 * @author richteri
 *
 */
public enum Column {
	NAME("(?i:(full)?\\s*name)", "name"),
	PHONE("(?i:(tele|mobile)?\\s*phone.*)", "phone_number"),
	ADDRESS("(?i:(full)?\\s*address)", "address"),
	CITY("(?i:(city|town))", "city"),
	STATE("(?i:state)", "state_code"),
	ZIP("(?i:(zip.*|postal\\s*code))", "postal_code");
	
	private String pattern;
	private String paramKey;
	
	
	Column(String pattern, String paramKey) {
		this.pattern = pattern;
		this.paramKey = paramKey;
	}

	/**
	 * Regular expression pattern for fuzzy name matching
	 * @return pattern
	 */
	public String getPattern() {
		return pattern;
	}
	
	/**
	 * WhitePages specific request parameter name
	 * @return parameter name
	 */
	public String getParamKey() {
		return paramKey;
	}
	
}
