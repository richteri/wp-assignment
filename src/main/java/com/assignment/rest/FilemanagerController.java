package com.assignment.rest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.assignment.domain.Record;
import com.assignment.service.RecordProcessorService;
import com.assignment.util.RecordProcessor;

/**
 * RESTful CSV file upload controller.<br>
 * Reads the CSV file line-by-line and processes and writes each record
 * parallelly into a temporary file.<br>
 * The processed temporary file will be exposed by a random UUID filename for
 * download.
 * 
 * @author richteri
 *
 */
@Controller
public class FilemanagerController {
	private final Logger log = LoggerFactory.getLogger(FilemanagerController.class);

	@Autowired
	private RecordProcessorService searchByService;

	/**
	 * Stores uploaded file in a temp directory
	 * @param file the file to persist
	 * @return location of the processed file
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/upload", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {

		// persist file
		if (!file.isEmpty()) {
			try {
				File processedFile = null;
				processedFile = Files.createTempFile(UUID.randomUUID().toString(), ".csv").toFile();
				processedFile.deleteOnExit();

				log.debug("File created for processing: {}", processedFile);

				FileWriter fileWriter = new FileWriter(processedFile);
				Writer writer = new PrintWriter(fileWriter, true);
				Reader reader = new InputStreamReader(file.getInputStream());

				try (RecordProcessor processor = new RecordProcessor(reader, writer)) {
				    log.debug("Record processor initialized");
					List<Future<?>> results = new ArrayList<>();

					// invoke parallel processing
					for (Record record; (record = processor.readRecord()) != null;) {
						results.add(searchByService.processRecord(record, processor));
						
						if (log.isDebugEnabled()) {
						    log.debug("Record added to the processing pool: {}", Arrays.toString(record.getRecord()));
						}
					}

					// wait for all threads
					results.forEach(result -> {
						try {
							result.get();
						} catch (Exception e) {
							log.warn("Processing record failed: {}", e.getMessage(), e);
						}
					});
					URI uri = URI.create("/download/" + processedFile.getName());
					return ResponseEntity.created(uri).body(null);
				} catch (Exception e) {
					log.warn("Processing file failed: {}", e.getMessage(), e);
					return ResponseEntity.badRequest().body("Processing file failed: " + e.getMessage());
				}
			} catch (IOException e) {
			    log.error("Processing file failed: {}", e.getMessage(), e);
			    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			}
		} else {
		    log.warn("The file is empty");
		    return ResponseEntity.badRequest().body("The file is empty");
		}
	}
	
	/**
	 * Exposes processed CSV file for download
	 * @param fileName the file to download
	 * @return file
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/download/{fileName:.+}", produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public ResponseEntity<FileSystemResource> handleFileDownload(@PathVariable String fileName) {
	    
	    log.debug("Downloading file {}", fileName);
		
		// disable caching
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		// serve file from temp dir as FileSystemResource
		try {
			Path tempFile = Files.createTempFile("temp-file-name", ".csv");
			File file = tempFile.getParent().resolve(fileName).toFile();
			Files.delete(tempFile);
			
			log.debug("Resolved file for download {}", file);
			
			FileSystemResource fsr = new FileSystemResource(file);
			
			return ResponseEntity.ok().headers(headers).contentLength(fsr.contentLength())
					.contentType(MediaType.parseMediaType("application/octet-stream"))
					.body(fsr);
		} catch (Exception e) {
		    log.warn("File not found {}", fileName);
			return new ResponseEntity<FileSystemResource>(HttpStatus.NOT_FOUND);
		}
	}
}
