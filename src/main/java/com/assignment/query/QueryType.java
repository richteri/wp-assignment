package com.assignment.query;

import com.assignment.response.Entity;
import com.assignment.response.Location;
import com.assignment.response.Phone;

/**
 * Enumerator for the necessary WhitePage services, endpoints and return types
 * @author richteri
 *
 */
public enum QueryType {
	PERSON("person.json", Entity.class),
	BUSINESS("business.json", Entity.class),
	LOCATION("location.json", Location.class),
	PHONE("phone.json", Phone.class);
	
	String path;
	Class<?> returnType;
	
	QueryType(String path, Class<?> returnType) {
		this.path = path;
		this.returnType = returnType;
	}
	
	public String getPath() {
		return path;
	}
	
	public Class<?> getReturnType() {
		return returnType;
	}
}
