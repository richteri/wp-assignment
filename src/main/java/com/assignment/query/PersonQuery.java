package com.assignment.query;

import org.springframework.core.ParameterizedTypeReference;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;
import com.assignment.response.Entity;
import com.assignment.response.GenericResponse;
import com.assignment.response.Location;
import com.assignment.response.Phone;

/**
 * Person query
 * @author richteri
 *
 */
public class PersonQuery extends GenericQuery<Entity> {

	public PersonQuery(WhitepagesProperties properties, Record record) {
		super(properties, QueryType.PERSON, record);
	}

	/*
	 * (non-Javadoc)
	 * @see com.assignment.query.GenericQuery#map(java.lang.Object)
	 */
	@Override
	public void map(Entity person) {
        if (person.getLocations() != null) {
            Location location = person.getLocations().get(0);
            getRecord().setAddress(location.getCity(), location.getStateCode(), location.getPostalCode());
        }
        
        if (person.getPhones() != null) {
            Phone phone = person.getPhones().get(0);
            getRecord().set(Column.PHONE, phone.getPhoneNumber());
        }
	}
	
    /* (non-Javadoc)
     * @see com.assignment.query.GenericQuery#typeReference()
     */
    @Override
    public ParameterizedTypeReference<GenericResponse<Entity>> typeReference() {
        return new ParameterizedTypeReference<GenericResponse<Entity>>(){};
    }


}
