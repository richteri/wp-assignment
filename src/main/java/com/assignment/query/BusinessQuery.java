package com.assignment.query;

import org.springframework.core.ParameterizedTypeReference;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;
import com.assignment.response.Entity;
import com.assignment.response.GenericResponse;
import com.assignment.response.Location;
import com.assignment.response.Phone;

/**
 * Business query
 * @author richteri
 *
 */
public class BusinessQuery extends GenericQuery<Entity> {

	public BusinessQuery(WhitepagesProperties properties, Record record) {
		super(properties, QueryType.BUSINESS, record);
	}

	/*
	 * (non-Javadoc)
	 * @see com.assignment.query.GenericQuery#map(java.lang.Object)
	 */
	@Override
	public void map(Entity business) {
        if (business.getLocations() != null) {
            Location location = business.getLocations().get(0);
            getRecord().setAddress(location.getCity(), location.getStateCode(), location.getPostalCode());
        }
        
        if (business.getPhones() != null) {
            Phone phone = business.getPhones().get(0);
            getRecord().set(Column.PHONE, phone.getPhoneNumber());
        }
	}
	
    /* (non-Javadoc)
     * @see com.assignment.query.GenericQuery#typeReference()
     */
    @Override
    public ParameterizedTypeReference<GenericResponse<Entity>> typeReference() {
        return new ParameterizedTypeReference<GenericResponse<Entity>>(){};
    }

}
