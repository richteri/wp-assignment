package com.assignment.query;

import java.net.URI;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;
import com.assignment.response.GenericResponse;

/**
 * A generic query class to provide common query functionalities regardless of query type
 * @author richteri
 * @param <T> Type of the expected query result
 */
public abstract class GenericQuery<T> {

    public static final String KEY_PROPERTY = "api_key";
    public static final String PAGE_LENGTH_PROPERTY = "page_len";
    public static final String PAGE_FIRST_PROPERTY = "page_first";

    private final WhitepagesProperties properties;
    private final QueryType queryType;
    private final Record record;

    public GenericQuery(WhitepagesProperties properties, QueryType queryType, Record record) {
        this.properties = properties;
        this.queryType = queryType;
        this.record = record;
    }

    public Record getRecord() {
        return record;
    }

    /**
     * API key is required. Always request only the first result.
     * @param params the parameter map to extend with defaults
     * @return extended parameter map
     */
    private MultiValueMap<String, String> addDefaultParams(
            MultiValueMap<String, String> params) {
        MultiValueMap<String, String> paramsWithKey = new LinkedMultiValueMap<String, String>(params);
        paramsWithKey.add(PAGE_FIRST_PROPERTY, "1");
        paramsWithKey.add(PAGE_LENGTH_PROPERTY, "1");
        paramsWithKey.add(KEY_PROPERTY, properties.getApi().getKey());
        return paramsWithKey;
    }

    /**
     * Builds a query type specific URI.
     * @param params the query parameters
     * @return query URI
     */
    public URI buildUri() {
        MultiValueMap<String, String> params = buildParams();
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(properties.getApi().getBaseUrl())
                .path(properties.getApi().getVersion())
                .path("/")
                .path(queryType.getPath())
                .queryParams(addDefaultParams(params)).build();
        return uriComponents.toUri();
    }

    /**
     * Populates all parameters that have a value.
     * @return query parameter map
     */
    public MultiValueMap<String, String> buildParams() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        if (record.has(Column.CITY)) {
            params.add(Column.CITY.getParamKey(), record.getCity());
        }
        if (record.has(Column.STATE)) {
            params.add(Column.STATE.getParamKey(), record.getState());
        }
        if (record.has(Column.ZIP)) {
            params.add(Column.ZIP.getParamKey(), record.getZip());
        }
        if (record.has(Column.ADDRESS)) {
            params.add(Column.ADDRESS.getParamKey(), record.getAddress());
        }
        if (record.has(Column.NAME)) {
            params.add(Column.NAME.getParamKey(), record.get(Column.NAME));
        }
        if (record.has(Column.PHONE)) {
            params.add(Column.PHONE.getParamKey(), record.get(Column.PHONE).replaceAll("[^0-9]", ""));
        }
        return params;
    }

    /**
     * Query type specific result-record value mapper.
     * @param entity the response entity
     */
    public abstract void map(T entity);
    
    /**
     * Spring hack to use RestTemplate with generics
     * @return the type reference
     */
    public abstract ParameterizedTypeReference<GenericResponse<T>> typeReference();
    
    /**
     * Executes the query
     * @param restTemplate the RESTful service client
     */
    public void execute(RestTemplate restTemplate) {
        
        ResponseEntity<GenericResponse<T>> response = restTemplate.exchange(
                buildUri(), HttpMethod.GET, null, typeReference());
                
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            
            if (response.getBody() != null 
                    && response.getBody().getResults() != null
                    && !response.getBody().getResults().isEmpty()) {
                // map only if the request was successful
                map(response.getBody().getResults().get(0));
            }
        }
    }
}
