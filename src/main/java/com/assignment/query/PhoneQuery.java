package com.assignment.query;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;
import com.assignment.response.Entity;
import com.assignment.response.GenericResponse;
import com.assignment.response.Location;
import com.assignment.response.Phone;

/**
 * Phone query
 * @author richteri
 *
 */
public class PhoneQuery extends GenericQuery<Phone> {

	public PhoneQuery(WhitepagesProperties properties, Record record) {
		super(properties, QueryType.PHONE, record);
	}

	/*
	 * (non-Javadoc)
	 * @see com.assignment.query.GenericQuery#map(java.lang.Object)
	 */
	@Override
	public void map(Phone phone) {
	    if (phone.getAssociatedLocations() != null) {
	        Location location = phone.getAssociatedLocations().get(0);
	        getRecord().setAddress(location.getCity(), location.getStateCode(), location.getPostalCode());
	    }
	    
	    if (phone.getBelongsTo() != null) {
	        Entity entity = phone.getBelongsTo().get(0);
	        getRecord().set(Column.NAME, entity.toName());
	    }
	}
	
    /* (non-Javadoc)
     * @see com.assignment.query.GenericQuery#typeReference()
     */
    @Override
    public ParameterizedTypeReference<GenericResponse<Phone>> typeReference() {
        return new ParameterizedTypeReference<GenericResponse<Phone>>(){};
    }

    /* (non-Javadoc)
     * @see com.assignment.query.GenericQuery#buildParams()
     */
    @Override
    public MultiValueMap<String, String> buildParams() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(Column.PHONE.getParamKey(), getRecord().get(Column.PHONE).replaceAll("[^0-9]", ""));
        return params;
    }

}
