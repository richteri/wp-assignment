package com.assignment.query;

import java.util.ArrayList;
import java.util.List;

import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;

/**
 * Builds queries for a specific record to complete missing properties
 * @author richteri
 *
 */
public class QueryBuilder {
	
    private QueryBuilder() {
    }
    
	/**
	 * Creates a prioritized list of queries to execute
	 * @param record the record to build queries for
	 * @return list of queries to run
	 */
	public static List<GenericQuery<?>> buildFor(WhitepagesProperties properties, Record record) {
		List<GenericQuery<?>> queries = new ArrayList<>();
		List<Column> missingFields = record.getMissingFields();
		List<Column> nonEmptyFields = record.getNonEmptyFields();
		
		// check if querying is necessary and possible
		if (missingFields.size() > 0 && nonEmptyFields.size() > 0 ) {
			if (nonEmptyFields.contains(Column.PHONE)) {
				queries.add(new PhoneQuery(properties, record));
			} else if (nonEmptyFields.contains(Column.NAME)) {
				queries.add(new PersonQuery(properties, record));
				queries.add(new BusinessQuery(properties, record));
			}
		}
		return queries;
	}
}
