package com.assignment.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * WhitePages phone query response entity
 * @author richteri
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Phone {
    
    @JsonProperty("phone_number")
	private String phoneNumber;
	
    @JsonProperty("belongs_to")
    private List<Entity> belongsTo;
	
    @JsonProperty("associated_locations")
    private List<Location> associatedLocations;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Entity> getBelongsTo() {
		return belongsTo;
	}

	public void setBelongsTo(List<Entity> belongsTo) {
		this.belongsTo = belongsTo;
	}

	public List<Location> getAssociatedLocations() {
		return associatedLocations;
	}

	public void setAssociatedLocations(List<Location> associatedLocations) {
		this.associatedLocations = associatedLocations;
	}

}
