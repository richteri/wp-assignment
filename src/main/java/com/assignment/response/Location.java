package com.assignment.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * WhitePages location response entity
 * @author richteri
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {
    
    @JsonProperty("type")
	private String type;
    
    @JsonProperty("city")
	private String city;
    
    @JsonProperty("postal_code")
	private String postalCode;
    
    @JsonProperty("state_code")
	private String stateCode;
    
    @JsonProperty("legal_entities_at")
	private List<Entity> legalEntitiesAt;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public List<Entity> getLegalEntitiesAt() {
		return legalEntitiesAt;
	}

	public void setLegalEntitiesAt(List<Entity> legalEntitiesAt) {
		this.legalEntitiesAt = legalEntitiesAt;
	}

}
