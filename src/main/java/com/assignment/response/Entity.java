package com.assignment.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * WhitePages response entity that merges business and person responses
 * @author richteri
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entity {

    @JsonProperty("name")
    private String name;
    
    @JsonProperty(value = "names")
	private List<Name> names;
    
    @JsonProperty("phones")
	private List<Phone> phones;
    
    @JsonProperty("locations")
	private List<Location> locations;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Name> getNames() {
		return names;
	}

	public void setNames(List<Name> names) {
		this.names = names;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	
	public String toName() {
	    if (name != null) {
	        return name;
	    }
	    
	    if (names != null && !names.isEmpty()) {
	        return names.get(0).toString();
	    }
	    
	    return "";
	}

}
