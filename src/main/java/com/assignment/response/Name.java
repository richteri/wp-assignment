package com.assignment.response;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * WhitePages specific names object used in person query responses
 * @author richteri
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Name {
    
    @JsonProperty("first_name")
	private String firstName;
    
    @JsonProperty("middle_name")
	private String middleName;
    
    @JsonProperty("last_name")
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return StringUtils.join(Arrays.asList(firstName, middleName, lastName), " ");
	}

	
}
