package com.assignment.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * US-based address parser
 * @author richteri
 *
 */
public class AddressParser {
	public static final Pattern STATE_RE = Pattern.compile("(^|\\s)([A-Z]{2})(,|\\s|$)");
	public static final Pattern ZIP_RE = Pattern.compile("(^|\\s)([0-9]{5}(-[0-9]{4})?)(|,|\\s|$)");
	public static final Pattern CLEANUP_ADDRESS_RE = Pattern.compile("(^[, ]+|[, ]+$|[, ]+)");
	
	private AddressParser() {
	}
	
	/**
	 * Try to extract 5 digit postal codes and optional 4 digit extensions from address string 
	 * @param address the address to process
	 * @return ZIP code if found
	 */
	public static String extractZip(String address) {
		Matcher matcher = ZIP_RE.matcher(address);
		if (matcher.find()) {
			return matcher.group(2);
		}
		
		return "";
	}
	
	/**
	 * Tries to extract 2 letter state abbreviations from address string 
	 * @param address the address to process
	 * @return the state code if found
	 */
	public static String extractState(String address) {
		Matcher matcher = STATE_RE.matcher(address);
		if (matcher.find()) {
			return matcher.group(2);
		}
		
		return "";
	}
	
	/**
	 * Separates ZIP and state code parts from address
	 * @param address the address to process
	 * @return address without state and ZIP info
	 */
	public static String extractAddress(String address) {
		address = address.replace(extractState(address), "");
		address = address.replace(extractZip(address), "");
		Matcher matcher = CLEANUP_ADDRESS_RE.matcher(address);
		return matcher.replaceAll(" ").trim();
	}
}
