package com.assignment.util;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.assignment.domain.Column;
import com.assignment.domain.Record;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * Helper class for CSV read - write
 * @author richteri
 *
 */
public class RecordProcessor implements AutoCloseable {

	private final CSVReader reader;
	private final CSVWriter writer;
	
	// first line of the file
	private final String[] header;
	
	// column - index mapping
	private final Map<Column, Integer> mapping;

	public RecordProcessor(Reader reader, Writer writer) throws IOException {
		this.reader = new CSVReader(reader);
		this.writer = new CSVWriter(writer);
		this.header = this.reader.readNext();
		this.writer.writeNext(header, false);
		this.mapping = detectMapping();
	}

	/**
	 * Parses first line as header and stores mapping information
	 * @return indexes mapped by columns 
	 */
	public final Map<Column, Integer> detectMapping() {
		Map<Column, Integer> mapping = new HashMap<>();
		for (int i = 0; i < header.length; i++) {
			for (Column column : Column.values()) {
				if (header[i].trim().matches(column.getPattern())) {
					mapping.put(column, i);
					break;
				}
			}
		}

		return Collections.unmodifiableMap(mapping);
	}

	/**
	 * Reads next line from CSV and converts it to an {@link Record} object
	 * @return converted record
	 * @throws IOException
	 */
	public Record readRecord() throws IOException {
		String[] record = reader.readNext();
		return (record != null) ? new Record(mapping, record) : null;
	}

	/**
	 * Persists an record in the CSV
	 * @param record
	 */
	public void writeRecord(Record record) {
		writer.writeNext(record.getRecord(), false);
	}

	@Override
	public void close() throws Exception {
		reader.close();
		writer.close();
	}

}
