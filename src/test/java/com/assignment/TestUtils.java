package com.assignment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

import com.assignment.domain.Record;
import com.assignment.util.RecordProcessor;

public class TestUtils {
    
    private TestUtils() {
    }

    public static Reader buildReader(String input) {
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());
        return new InputStreamReader(inputStream);
    }

    public static Writer buildWriter() {
        return new PrintWriter(System.out, true);
    }
    
    public static RecordProcessor buildProcessor(String csv) {
        try {
            RecordProcessor entityProcessor = new RecordProcessor(buildReader(csv), buildWriter());
            return entityProcessor;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }
    
    public static Record buildRecord(String csv) {
        try (RecordProcessor processor = buildProcessor(csv)) {
            return processor.readRecord();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

}
