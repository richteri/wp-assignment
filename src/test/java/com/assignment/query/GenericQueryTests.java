package com.assignment.query;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.AssignmentApplication;
import com.assignment.WhitepagesProperties;
import com.assignment.domain.Column;
import com.assignment.domain.Record;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AssignmentApplication.class)
public class GenericQueryTests {
	public static final String URL_PERSON = "https://proapi.whitepages.com/2.2/person.json";
	public static final String URL_BUSINESS = "https://proapi.whitepages.com/2.2/business.json";
	public static final String URL_LOCATION = "https://proapi.whitepages.com/2.2/location.json";
	public static final String URL_PHONE = "https://proapi.whitepages.com/2.2/phone.json";

	
	@Autowired
	private WhitepagesProperties properties;
	
	@Test
	public void testPersonUrlBuilder() {
		GenericQuery<?> query = new PersonQuery(properties, new Record(new HashMap<Column, Integer>(), null));
		assertThat(query.buildUri().toString(), startsWith(URL_PERSON));
		assertThat(query.buildUri().toString(), endsWith("key=" + properties.getApi().getKey()));
	}
	
	@Test
	public void testBusinessUrlBuilder() {
		GenericQuery<?> query = new BusinessQuery(properties, new Record(new HashMap<Column, Integer>(), null));
		assertThat(query.buildUri().toString(), startsWith(URL_BUSINESS));
		assertThat(query.buildUri().toString(), endsWith("key=" + properties.getApi().getKey()));
	}
	
	@Test
	public void testPhoneUrlBuilder() {		
		GenericQuery<?> query = new PhoneQuery(properties, new Record(new HashMap<Column, Integer>(), null));
		assertThat(query.buildUri().toString(), startsWith(URL_PHONE));
	}
}
