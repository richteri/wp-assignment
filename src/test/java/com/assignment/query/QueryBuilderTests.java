package com.assignment.query;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.AssignmentApplication;
import com.assignment.TestUtils;
import com.assignment.WhitepagesProperties;
import com.assignment.domain.Record;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AssignmentApplication.class)
public class QueryBuilderTests {

    public static final String CSV_EMPTY = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,,\"\", ,,";
    public static final String CSV_FULL_ADDRESS = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Fő utca 1., Balatonfüred, VP 82300\", +36 12/345-56-78,,";
    public static final String CSV_FULL_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,Richter István,Balatonfüred, +36 12/345-56-78,,8230";
    public static final String CSV_CITY_STATE_ZIP_NO_PHONE = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,Richter István,Balatonfüred, ,,8230";
    public static final String CSV_ADDRESS_MISSING_PHONE = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Balatonfüred, Veszprém, 8230\",,,";
    public static final String CSV_PHONE_NO_NAME = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,,Balatonfüred, +36 12/345-56-78,,8230";
    public static final String CSV_ADDRESS_ONLY = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n, ,\"Fő utca 1., Balatonfüred, VP 82300\", ,,";
    public static final String CSV_CITY_STATE_ZIP_ONLY = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,,Balatonfüred, ,,8230";
    
    @Autowired
    private WhitepagesProperties properties;

	@Test
	public void testBuildForEmpty() {
		Record record = TestUtils.buildRecord(CSV_EMPTY);
		List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
		assertTrue(queries.isEmpty());
	}
	
    @Test
    public void testBuildForFullAddress() {
        Record record = TestUtils.buildRecord(CSV_FULL_ADDRESS);
        List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
        assertTrue(queries.isEmpty());
    }

    @Test
    public void testBuildForFullCityStateZip() {
        Record record = TestUtils.buildRecord(CSV_FULL_CITY_STATE_ZIP);
        List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
        assertTrue(queries.isEmpty());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testBuildForPhone() {
        Record record = TestUtils.buildRecord(CSV_CITY_STATE_ZIP_NO_PHONE);
        List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
        assertThat(queries.size(), equalTo(2));
        assertThat(queries, contains(instanceOf(PersonQuery.class), instanceOf(BusinessQuery.class)));
    }

    @Test
    public void testBuildForName() {
        Record record = TestUtils.buildRecord(CSV_PHONE_NO_NAME);
        List<GenericQuery<?>> queries = QueryBuilder.buildFor(properties, record);
        assertThat(queries.size(), equalTo(1));
        assertThat(queries, contains(instanceOf(PhoneQuery.class)));
    }

}
