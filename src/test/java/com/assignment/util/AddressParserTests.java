package com.assignment.util;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class AddressParserTests {
	public static final String SAMPLE_CITY_STATE_ZIP_01 = "Mapple Valley, WA 98038";
	public static final String SAMPLE_CITY_STATE_ZIP_02 = "421 E DRACHMAN TUCSON AZ 85705-7598";

	@Test
	public void testExtractZip() {
		assertThat(AddressParser.extractZip(SAMPLE_CITY_STATE_ZIP_01), equalTo("98038"));
		assertThat(AddressParser.extractZip(SAMPLE_CITY_STATE_ZIP_02), equalTo("85705-7598"));
	}

	@Test
	public void testExtractState() {
		assertThat(AddressParser.extractState(SAMPLE_CITY_STATE_ZIP_01), equalTo("WA"));
		assertThat(AddressParser.extractState(SAMPLE_CITY_STATE_ZIP_02), equalTo("AZ"));
	}

	@Test
	public void testExtractAddress() {
		assertThat(AddressParser.extractAddress(SAMPLE_CITY_STATE_ZIP_01), equalTo("Mapple Valley"));
		assertThat(AddressParser.extractAddress(SAMPLE_CITY_STATE_ZIP_02), equalTo("421 E DRACHMAN TUCSON"));
	}
	
	

}
