package com.assignment.util;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.OutputCapture;

import com.assignment.TestUtils;
import com.assignment.domain.Column;
import com.assignment.domain.Record;

public class RecordProcessorTests {

	public static final String CSV_ADDRESS = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Balatonfüred, Veszprém, 8230\", +36 12/345-56-78,,";
	public static final String CSV_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,Richter István,Balatonfüred, +36 12/345-56-78,,8230";
	public static final String CSV_ADDRESS_MISSING_PHONE = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Balatonfüred, Veszprém, 8230\",,,";
	
	
	@Rule
	public OutputCapture capture = new OutputCapture();

	@Test
	public void testDetectMappingWithAddress() throws Exception {
		RecordProcessor entityProcessor = TestUtils.buildProcessor(CSV_ADDRESS);
		Map<Column, Integer> mapping = entityProcessor.detectMapping();
		assertThat(mapping.keySet(), containsInAnyOrder(Column.NAME, Column.ADDRESS, Column.PHONE));
		assertThat(mapping.keySet(), not(containsInAnyOrder(Column.CITY, Column.STATE, Column.ZIP)));
		entityProcessor.close();
	}

	@Test
	public void testDetectMappingWithCityStateZip() throws Exception {
	    RecordProcessor entityProcessor = TestUtils.buildProcessor(CSV_CITY_STATE_ZIP);
		Map<Column, Integer> mapping = entityProcessor.detectMapping();
		assertThat(mapping.keySet(),
				containsInAnyOrder(Column.NAME, Column.CITY, Column.STATE, Column.ZIP, Column.PHONE));
		assertThat(mapping.keySet(), not(containsInAnyOrder(Column.ADDRESS)));
		entityProcessor.close();
	}

	@Test
	public void testReadEntity() {
		Record record = TestUtils.buildRecord(CSV_ADDRESS_MISSING_PHONE);
		assertThat(record.get(Column.NAME), equalTo("Richter István"));
		assertThat(record.get(Column.ADDRESS), equalTo("Balatonfüred, Veszprém, 8230"));
		assertThat(record.get(Column.PHONE), isEmptyString());
		assertThat(record.get(Column.CITY), isEmptyString());
		assertThat(record.get(Column.STATE), isEmptyString());
		assertThat(record.get(Column.ZIP), isEmptyString());
	}
	
	@Test
	public void testWriteEntity() throws Exception {
	    RecordProcessor entityProcessor = TestUtils.buildProcessor(CSV_ADDRESS_MISSING_PHONE);
		Record entity = entityProcessor.readRecord();
		entityProcessor.writeRecord(entity);
		
		capture.expect(equalTo("Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Balatonfüred, Veszprém, 8230\",,,\n"));
		entityProcessor.close();
	}
	
}
