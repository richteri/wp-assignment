package com.assignment.service;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.concurrent.Future;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.OutputCapture;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.AssignmentApplication;
import com.assignment.TestUtils;
import com.assignment.domain.Record;
import com.assignment.util.RecordProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AssignmentApplication.class)
public class RecordProcessorServiceTests {

    // phone query
    public static final String CSV_FIND_PERSON_ADDRESS_BY_PHONE = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,,, 206-973-5184,,";
    public static final String CSV_FIND_BUSINESS_ADDRESS_BY_PHONE = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,,, 206-973-5100,,";
    public static final String CSV_FIND_PERSON_CITY_STATE_ZIP_BY_PHONE = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \n,,, (206) 973-5184,,";
    public static final String CSV_FIND_BUSINESS_CITY_STATE_ZIP_BY_PHONE = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \n,,, (206) 973-5100,,";

    // person and business queries
    public static final String CSV_FIND_PERSON_BY_NAME_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nMT,Drama Number,Ashland,,,";
    public static final String CSV_FIND_BUSINESS_BY_NAME_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nWA,Starbucks,Seattle,,,";

    @Rule
    public OutputCapture capture = new OutputCapture();
    
    @Autowired
    private RecordProcessorService service;
    
	@Test
	public void testFindPersonAddressByPhone() throws Exception {
	    RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_PERSON_ADDRESS_BY_PHONE);
	    Record record = processor.readRecord();
	    
	    Future<?> future = service.processRecord(record, processor);
	    future.get();	    
	    
	    capture.expect(containsString(",Robert E Noble,\"Seattle, WA 98115\", 206-973-5184,,"));
	    
	    processor.close();
	}

    @Test
    public void testFindBusinessAddressByPhone() throws Exception {
        RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_BUSINESS_ADDRESS_BY_PHONE);
        Record record = processor.readRecord();
        
        Future<?> future = service.processRecord(record, processor);
        future.get();       
        
        capture.expect(containsString(",Whitepages,\"Seattle, WA 98115\", 206-973-5100,,"));
        
        processor.close();
    }
    
    @Test
    public void testFindPersonCityStateZipByPhone() throws Exception {
        RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_PERSON_CITY_STATE_ZIP_BY_PHONE);
        Record record = processor.readRecord();
        
        Future<?> future = service.processRecord(record, processor);
        future.get();       
        
        capture.expect(containsString("WA,Robert E Noble,Seattle, (206) 973-5184,,98115"));
        
        processor.close();
    }

    @Test
    public void testFindBusinessCityStateZipByPhone() throws Exception {
        RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_BUSINESS_CITY_STATE_ZIP_BY_PHONE);
        Record record = processor.readRecord();
        
        Future<?> future = service.processRecord(record, processor);
        future.get();       
        
        capture.expect(containsString("WA,Whitepages,Seattle, (206) 973-5100,,98115"));
        
        processor.close();
    }
    
    @Test
    public void testFindPersonByNameCityStateZip() throws Exception {
        RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_PERSON_BY_NAME_CITY_STATE_ZIP);
        Record record = processor.readRecord();
        
        Future<?> future = service.processRecord(record, processor);
        future.get();       
        
        capture.expect(containsString("MT,Drama Number,Ashland,6464806649,,59004"));
        
        processor.close();
    }

    @Test
    public void testFindBusinessByNameCityStateZip() throws Exception {
        RecordProcessor processor = TestUtils.buildProcessor(CSV_FIND_BUSINESS_BY_NAME_CITY_STATE_ZIP);
        Record record = processor.readRecord();
        
        Future<?> future = service.processRecord(record, processor);
        future.get();       
        
        capture.expect(containsString("WA,Starbucks,Seattle,2063286920,,98102"));
        
        processor.close();
    }

}
