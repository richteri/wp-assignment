package com.assignment.domain;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import com.assignment.TestUtils;

public class RecordTests {
    private static final String CSV_EMPTY_ADDRESS = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,,\"\", ,,";
    private static final String CSV_EMPTY_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \n,,, ,,";
    private static final String CSV_FULL_ADDRESS = "Custom01, Name ,Address  ,Phone number ,Custom02,Custom03\n,Richter István,\"Fő utca 1., Balatonfüred, VP 82300\", +36 12/345-56-78,,";
    private static final String CSV_FULL_CITY_STATE_ZIP = " State ,  Name ,City  ,Phone number  ,Custom02 ,ZIP  \nVeszprém,Richter István,Balatonfüred, +36 12/345-56-78,,8230";
    
    @Test
    public void testEmptyAddress() {
        Record record = TestUtils.buildRecord(CSV_EMPTY_ADDRESS);
        assertThat(record.getMissingFields(), containsInAnyOrder(Column.NAME, Column.ADDRESS, Column.PHONE));
        assertTrue(record.getNonEmptyFields().isEmpty());
    }
    
    @Test
    public void testEmptyCityStateZip() {
        Record record = TestUtils.buildRecord(CSV_EMPTY_CITY_STATE_ZIP);
        assertThat(record.getMissingFields(), containsInAnyOrder(Column.NAME, Column.STATE, Column.CITY, Column.ZIP, Column.PHONE));
        assertTrue(record.getNonEmptyFields().isEmpty());
    }
    
    @Test
    public void testFullAddress() {
        Record record = TestUtils.buildRecord(CSV_FULL_ADDRESS);
        assertThat(record.getNonEmptyFields(), containsInAnyOrder(Column.NAME, Column.ADDRESS, Column.PHONE));
        assertTrue(record.getMissingFields().isEmpty());
        
        assertThat(record.getZip(), equalTo("82300"));
        assertThat(record.getState(), equalTo("VP"));
        assertThat(record.getAddress(), equalTo("Fő utca 1. Balatonfüred"));
    }
    
    @Test
    public void testFullCityStateZip() {
        Record record = TestUtils.buildRecord(CSV_FULL_CITY_STATE_ZIP);
        assertThat(record.getNonEmptyFields(), containsInAnyOrder(Column.NAME, Column.STATE, Column.CITY, Column.ZIP, Column.PHONE));
        assertTrue(record.getMissingFields().isEmpty());
    }
}
